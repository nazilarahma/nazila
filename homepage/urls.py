from django.urls import path
from . import views
from django.conf.urls import url

app_name = 'homepage'

urlpatterns = [
    path('', views.index, name='index'),
    path('mine/', views.mine, name='mine'),
    path('new/', views.post_new, name='post_new'),
    path('show/', views.post_list, name='post_list'),
    
    
    url(r'show/(?P<pk>[0-9]+)/delete/$', views.Delete.as_view(),name='delete'),
]