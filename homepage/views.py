from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import PostForm
from .models import Post
from django.urls import reverse_lazy
from django.views.generic.edit import DeleteView



def index(request):
    return render(request, 'index.html')
def mine(request):
    return render(request, 'mine.html')
def post_list(request):
    postlist = Post.objects.all()
    return render(request, 'post_list.html', {'postlist': postlist})    
    
def post_new(request):
    if request.method == "POST":
           form = PostForm(request.POST)
           if form.is_valid():
               post = form.save()
               post.save()
               return redirect('/show/')
    else:
        form = PostForm()
    return render(request, 'post_edit.html', {'form': form})
    

class Delete(DeleteView):
    model=Post
    success_url=reverse_lazy('homepage:post_list')