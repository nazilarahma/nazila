from django.conf import settings
from django.db import models
from django.utils import timezone


class Post(models.Model):
    hari= models.CharField(max_length=10)
    tanggal= models.DateField()
    jam= models.TimeField()
    namaKegiatan = models.CharField(max_length=200)
    tempat = models.CharField(max_length=50)
    kategori= models.CharField(max_length=20)
    

    def __str__(self):
        return self.namaKegiatan
        
    def deftanggal(self):
        return self.tanggal.strftime('%d')
    def bulan(self):
        return self.tanggal.strftime('%b')
   