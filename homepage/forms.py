from django import forms
from .models import Post

class PostForm(forms.ModelForm):

    class Meta:
        model = Post
        fields = ['hari', 'tanggal', 'jam', 'namaKegiatan', 'tempat', 'kategori']
        widgets = {
                    'hari': forms.TextInput(),
                    'tanggal': forms.TextInput(attrs={'placeholder': 'YYYY-MM-DD'}),
                    'jam': forms.TextInput(attrs={'placeholder': 'HH-MM'}),
                    'namaKegiatan': forms.TextInput(), 'tempat':forms.TextInput(), 'kategori':forms.TextInput()
                }
        
        
        
    def __init__(self, *args, **kwargs):
        super(PostForm,self).__init__(*args,**kwargs)
        for field in self.Meta.fields:
            self.fields[field].widget.attrs.update({
                'class':'form-control'
            })
        